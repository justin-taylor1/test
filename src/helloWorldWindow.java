import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.Color;
import javax.swing.SwingConstants;

public class helloWorldWindow {

	private JFrame frame;
	private JTextField nameTextField;

	/**
	 * Launch the application. n
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					helloWorldWindow window = new helloWorldWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public helloWorldWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setForeground(Color.WHITE);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton nameButton = new JButton("New button");
		nameButton.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 14));
		nameButton.setBackground(Color.BLACK);
		nameButton.setForeground(Color.BLACK);
		nameButton.setBounds(162, 165, 131, 23);
		frame.getContentPane().add(nameButton);
		
		nameTextField = new JTextField();
		nameTextField.setBackground(Color.BLACK);
		nameTextField.setHorizontalAlignment(SwingConstants.CENTER);
		nameTextField.setForeground(Color.RED);
		nameTextField.setFont(new Font("Tahoma", Font.PLAIN, 18));
		nameTextField.setText("Start");
		nameTextField.setBounds(64, 90, 312, 49);
		frame.getContentPane().add(nameTextField);
		nameTextField.setColumns(10);
	}
}
